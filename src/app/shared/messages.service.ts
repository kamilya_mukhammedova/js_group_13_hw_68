import { Injectable } from '@angular/core';
import { Message } from './message.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Injectable()
export class MessagesService {
  messagesChange = new Subject<Message[]>();
  messagesFetching = new Subject<boolean>();
  messagesUploading = new Subject<boolean>();
  date = '';
  interval = 0;

  private messagesArray: Message[] = [];

  constructor(private http: HttpClient) {}

  fetchMessages() {
    this.messagesFetching.next(true);
    this.http.get<Message[]>('http://146.185.154.90:8000/messages')
      .pipe(map(result => {
        if (result.length === 0) {
          return [];
        }
        return result.map(message => {
          return new Message(message.id, message.message, message.author, message.datetime);
        });
      }))
      .subscribe(messages => {
        this.messagesArray = messages;
        this.messagesChange.next(this.messagesArray.slice());
        this.date = this.messagesArray[this.messagesArray.length - 1].datetime;
        this.messagesFetching.next(false);
      }, () => {
        this.messagesFetching.next(false);
      });
  }

  addMessage(message: Message) {
    const body = new HttpParams()
      .set('author', message.author)
      .set('message', message.message);
    this.messagesUploading.next(true);
    return this.http.post('http://146.185.154.90:8000/messages', body)
      .pipe(tap(() => {
        this.messagesUploading.next(false);
      }, () => {
        this.messagesUploading.next(false);
      }));
  }

  start() {
    if(this.interval !== 0) {
      return;
    }
    this.interval = setInterval(() => {
      this.http.get<Message[] | []>(`http://146.185.154.90:8000/messages?datetime=${this.date}`)
        .pipe(map(result => {
          if(result.length === 0) {
            return [];
          }
          return result.map(message => {
            return new Message(message.id, message.message, message.author, message.datetime);
          });
        }))
        .subscribe(newMessages => {
          this.messagesArray = this.messagesArray.concat(newMessages);
          this.date = this.messagesArray[this.messagesArray.length - 1].datetime;
          this.messagesChange.next(this.messagesArray.slice());
        }, () => {
          console.log('Something has gone wrong with new messages in chat.');
        });
    },2000);
  }

  stop() {
    clearInterval(this.interval);
    this.interval = 0;
  }
}

