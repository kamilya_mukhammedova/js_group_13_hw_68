import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MessagesService } from '../shared/messages.service';
import { Subscription } from 'rxjs';
import { Message } from '../shared/message.model';

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.component.html',
  styleUrls: ['./new-message.component.css']
})
export class NewMessageComponent implements OnInit, OnDestroy {
  @ViewChild('f') messageForm!: NgForm;
  messagesUploadingSubscription!: Subscription;
  isUploading = false;

  constructor(private messagesService: MessagesService) { }

  ngOnInit(): void {
    this.messagesUploadingSubscription = this.messagesService.messagesUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });
  }

  sendMessage() {
    if(this.messageForm.value.message === '' || this.messageForm.value.author === '') {
      alert('You need to fill all the fields');
    }
      const id = Math.random().toString();
      const date = new Date().toString();
      const newMessage = new Message(
        id,
        this.messageForm.value.message,
        this.messageForm.value.author,
        date
      );
      this.messagesService.addMessage(newMessage).subscribe(() => {
        this.clearFormValue({
          message: '',
          author: '',
        });
      });
  }

  clearFormValue(value: {[key: string]: any}) {
    setTimeout(() => {
      this.messageForm.setValue(value);
    });
  }

  ngOnDestroy(): void {
    this.messagesUploadingSubscription.unsubscribe();
  }
}
